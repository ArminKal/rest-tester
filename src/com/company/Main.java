package com.company;

import java.io.*;
import java.net.*;

import com.tridium.json.JSONArray;
import com.tridium.json.JSONException;
import com.tridium.json.JSONObject;

public class Main {

    private static HttpURLConnection m_HttpConnection;
    private static String m_LastKnownError;
    private static boolean m_IsControllerOnline;
    private static String m_HttpApiToken = "cbRFYwSGga5XZpvnhUkBB8kRsncPJcAG7jyuxFVPajb9CEkpjbrZ3z2qsBkKKA9gqzRT8BaGNL6aamFwkU2FvNSNtyPu3WKwRd57G9U9SSUSduYWd9cZNS4QBkxHTHEX";
    private static String m_HttpAdress = "externalapi.eocharging.com";
    private static boolean m_IsConnectInProgress = false;

    public static void main(String[] args) throws Exception {
	// write your code here
        URL url;
        url = new URL("https", m_HttpAdress,  "/hub/status?chargepoints=1");
        System.out.println(" GET STATUS URL: " + url.toString());
        JSONObject jsonPayload = httpGet(url);
        System.out.println(jsonPayload);
    }

    private static HttpURLConnection setBody(String params, HttpURLConnection HttpConnection) throws Exception {
        try(OutputStream os = HttpConnection.getOutputStream()) {
            byte[] input = params.getBytes("utf-8");
            os.write(input, 0, input.length);
            return HttpConnection;
        }
//        OutputStream os = m_HttpConnection.getOutputStream();
//        OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
//        byte[] bytes = params.getBytes("UTF-8");
//        osw.write(String.valueOf(bytes));
//        osw.flush();
//        osw.close();

    }

    private static JSONObject httpGet(URL url) throws Exception {

        JSONObject jOPayload = null;
        try {
            m_HttpConnection = (HttpURLConnection) url.openConnection();
            m_HttpConnection.setRequestMethod("GET");
            urlSetDefaults();
            m_HttpConnection.setDoOutput(true);

//            if(bodyParams != null && !bodyParams.isEmpty()) {
//                m_HttpConnection = setBody(bodyParams, m_HttpConnection);
//            }

            int httpResponseCode = m_HttpConnection.getResponseCode();

            System.out.println("httpResponseCode for " + url + " :    " + httpResponseCode);

            if (httpResponseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(m_HttpConnection.getInputStream()));
                String inputLine;
                StringBuffer content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }

                if (content != null) {
                    String payload = content.toString();
                    if(isJsonObject(payload)) {
                        jOPayload = new JSONObject(payload);

                    }else if(isJsonArray(payload)){
                        JSONArray jA = new JSONArray(payload);
                        jOPayload = new JSONObject();
                        jOPayload.put("response", jA);  // Add a RESPONSE field we what to always return a JSONObject
                    }else{
                        m_LastKnownError = "Unknown HTTP repsonse";
                    }
                }
            } else if (httpResponseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                m_LastKnownError = "Unauthorised access - missing or invalid authentication token";
                m_IsControllerOnline = false;
            } else if (httpResponseCode == HttpURLConnection.HTTP_FORBIDDEN) {
                m_LastKnownError = "Permission denied - missing or invalid authentication token";
                m_IsControllerOnline = false;
            } else if (httpResponseCode == HttpURLConnection.HTTP_NOT_FOUND) {
                m_LastKnownError = "Target not found" ;
                m_IsControllerOnline = false;
            } else if (httpResponseCode == HttpURLConnection.HTTP_INTERNAL_ERROR) {
                m_LastKnownError = "Unable to contact - internal error";
                m_IsControllerOnline = false;
            } else if (httpResponseCode == HttpURLConnection.HTTP_BAD_REQUEST) {
                m_LastKnownError = "Bad request - invalid filter";
                m_IsControllerOnline = false;
            } else {
                m_LastKnownError = "HTTP Error: " + httpResponseCode;
                m_IsControllerOnline = false;
            }

        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            m_LastKnownError = "HTTP timeout" + " -> " +e.getMessage();
            m_IsControllerOnline = false;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            m_LastKnownError = "HTTP Malformed URL" + " -> " +e.getMessage();
            m_IsControllerOnline = false;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            m_LastKnownError = "HTTP Unknown Exception" + " -> " +e.getMessage();
            m_IsControllerOnline = false;
        } catch (IOException e) {
            e.printStackTrace();
            m_LastKnownError = "HTTP IO Exception" + " -> " +e.getMessage();
            m_IsControllerOnline = false;
        } catch (Exception e) {
            e.printStackTrace();
            m_LastKnownError = "HTTP Exception" + " -> " +e.getMessage();
            m_IsControllerOnline = false;
        }

        return jOPayload;
    }

    private static void urlSetDefaults() {
        m_HttpConnection.setRequestProperty("Accept", "application/json");
        m_HttpConnection.setRequestProperty("Content-Type", "application/json; utf-8");
        m_HttpConnection.setRequestProperty("x-api-key", m_HttpApiToken);
        m_HttpConnection.setConnectTimeout(5000); // Connect milli seconds
        m_HttpConnection.setReadTimeout(5000); // Read timer milli seconds
    }

    private static boolean isJsonObject(String value) {
        try {
            new JSONObject(value);
        }catch (JSONException ex) {
            return false;
        }
        return true;
    }

    private static boolean isJsonArray(String value) {
        try {
            new JSONArray(value);
        } catch (JSONException ex) {
            return false;
        }
        return true;
    }


}
